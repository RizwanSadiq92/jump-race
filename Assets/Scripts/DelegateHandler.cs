﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelegateHandler
{
    public delegate void onCompleteDelegate();
    public static onCompleteDelegate GameComplete;

    public static void OnGameComplete()
    {
        if (GameComplete != null)
            GameComplete();
    }
}
