﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public Transform NextPlatform;
    public int platformNo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }

   public void AimNextPlatform(GameObject characterTriggered)
    {
        if (NextPlatform != null)
            characterTriggered.GetComponent<CharacterMovement>().CharacterLookAtTarget(NextPlatform);


    }
}
