﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    public Platform parent;
    int delay=1;
    private void Start()
    {
        parent = transform.GetComponentInParent<Platform>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        
        if(collision.transform.tag == "Player")
        {
            FindObjectOfType<UIController>().SetGameProgress(parent.platformNo);
            if (delay % 2 == 0)
            parent.AimNextPlatform(collision.gameObject);
            delay++;
        }
    }
}
