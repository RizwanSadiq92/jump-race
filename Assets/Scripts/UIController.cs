﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    public GameObject CompletePanel;
    public Slider gameProgress;


    private void OnEnable()
    {
        DelegateHandler.GameComplete += ShowPanel;
    }

    private void OnDisable()
    {
        DelegateHandler.GameComplete -= ShowPanel;
    }

    public void ShowPanel()
    {
        StartCoroutine(waitForPanelComplete());
    }

    IEnumerator waitForPanelComplete()
    {
        yield return new WaitForSecondsRealtime(0.2f);
        CompletePanel.SetActive(true);
    }

    public void Restart()
    {
        SceneManager.LoadScene(0);
    }

    public void SetGameProgress(int platformNo)
    {
        if(platformNo != 0)
            gameProgress.value = (float)platformNo / 19;
    }
}
