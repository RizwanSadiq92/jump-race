﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject target,Player;

    public float smoothSpeed = 0.125f;
    public Vector3 offset;
    public float distance;

    public bool isMovingForward;

    //Will Follow player and Always make sure that camera is point at the back
    void FixedUpdate()
    {

        Vector3 desiredPosition = target.transform.position + offset;
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed); // will Lerp  the camera to desired Position

        transform.position = smoothedPosition;
        transform.LookAt(Player.transform.position); // camera will always look at player
    }

}
