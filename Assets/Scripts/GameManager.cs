﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{


    private void OnEnable()
    {
        DelegateHandler.GameComplete += GameComplete;
    }

    private void OnDisable()
    {
        DelegateHandler.GameComplete -= GameComplete;
    }
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1;
    }

    public void GameComplete()
    {
        Time.timeScale = 0;
    }

    public void GameFail()
    {
        Time.timeScale = 0;
    }
}
