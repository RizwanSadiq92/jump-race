﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DepthMeasure : MonoBehaviour
{

    public LineRenderer laserLineRenderer;
    public float laserMaxLength = 1f;

    public Color inRange, outOfRange;

    void Update()
    {
        laserLineRenderer.SetPosition(0, transform.position);
        laserLineRenderer.SetPosition(1, DetectHit(transform.position, laserMaxLength, Vector3.down));
    }

    Vector3 DetectHit(Vector3 startPos, float distance, Vector3 direction)
    {
        Ray ray = new Ray(startPos, direction);
        RaycastHit hit;
        Vector3 endPos = startPos + (distance * direction);

        if (Physics.Raycast(ray, out hit, distance) && hit.collider.transform.tag !="Water")
        {
            endPos = hit.point;
            laserLineRenderer.material.color = inRange;
            return endPos;
        }
        else
        {
            laserLineRenderer.material.color = outOfRange;
        }

        return endPos;
    }

}
