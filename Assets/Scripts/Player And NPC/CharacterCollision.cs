﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterMovement))]
public class CharacterCollision : MonoBehaviour
{
    public Animator characterAnimator;

    //[SerializeField]
    protected CharacterMovement characterMovement;



    private void OnEnable()
    {
        
    }


    private void Start()
    {
        characterMovement = GetComponent<CharacterMovement>();
    }
    private void OnCollisionEnter(Collision collision)
    {

        if (collision.transform.tag == "Water")
        {
            //FindObjectOfType<GameManager>().GameFail();
            DelegateHandler.OnGameComplete();
        }
        else if (collision.transform.tag == "Finish")
        {
            DelegateHandler.OnGameComplete();
            //FindObjectOfType<GameManager>().GameComplete();
        }

        if (collision.transform.tag == "Platform")
        {
            characterMovement.BoostSpeed = 800;
            characterMovement.ContinouseJump();
            characterAnimator.SetTrigger("Jump");
            characterAnimator.SetFloat("Blend", Random.Range(0, 2));
        }
        else if (collision.transform.tag == "Platform2")
        {
            Debug.Log("LongJump");
            characterMovement.BoostSpeed = 1800;
            characterMovement.ContinouseJump();
            characterAnimator.SetTrigger("Jump");
            characterAnimator.SetFloat("Blend", Random.Range(0, 2));
        }
    }
}
