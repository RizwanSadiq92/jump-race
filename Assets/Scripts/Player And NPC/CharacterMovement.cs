﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour
{

    [SerializeField]
    protected float rotationSpeed =0.7f;

    [SerializeField]
    protected float forwardSpeed = 4f;

    [SerializeField]
    protected Rigidbody rigidbody;

    [SerializeField]
    protected Vector3 direction = Vector3.up;

    [SerializeField]
    public float maxSpeed = 43.1f;

    private int tempBoost;

    [SerializeField]
    protected int boostSpeed;


    private void Start()
    {
        tempBoost = boostSpeed;
        rigidbody = GetComponent<Rigidbody>();
    }

    #region Movement Controls

    // When Called will Allow Player To Move Forward In Z Direction with Specific Speed
    public void ForwardMovement()
    {

        transform.position += transform.forward * Time.deltaTime * forwardSpeed;
    }

    // This Function will rotate the object around Y-Axis and camera will automatically follow
    public void SideControl()
    {
        RotateCharacter();
    }

    // Rotation On Bases of Finger Drag Movement
    void RotateCharacter()
    {

#if UNITY_EDITOR

        float XaxisRotation = -Input.GetAxis("Mouse X") * rotationSpeed * 3 * Time.smoothDeltaTime;
#else

        float XaxisRotation = -Input.GetAxis("Mouse X") * rotationSpeed * Time.smoothDeltaTime;

#endif


        // select the axis by which you want to rotate the GameObject
        transform.RotateAround(Vector3.down, XaxisRotation);
    }

    public void RotateCharacter(float rotateAxis)
    {
        float XaxisRotation = rotateAxis * rotationSpeed * Time.smoothDeltaTime;
        // select the axis by which you want to rotate the NPC
        transform.RotateAround(Vector3.down, XaxisRotation);
    }

    public void CharacterLookAtTarget(Transform targetPlatform)
    {
        Vector3 tempTargetPlatfrom = new Vector3(targetPlatform.position.x,targetPlatform.position.y, targetPlatform.position.z);

        transform.LookAt(tempTargetPlatfrom);
        transform.rotation = new Quaternion(0, transform.rotation.y, 0, 1);
    }

#endregion Movement Controls


#region Continous Jumping

    public void ContinouseJump()
    {
        rigidbody.velocity = Vector3.zero;

        if (Vector3.Project(rigidbody.velocity, direction).magnitude < maxSpeed)
        {
            rigidbody.AddForce(direction * boostSpeed);
        }
    }


#endregion Continous Jumping

#region Getter Setter

    public int BoostSpeed
    {
        get { return boostSpeed; }
        set { boostSpeed = value;}
    }

#endregion Getter Setter
}
