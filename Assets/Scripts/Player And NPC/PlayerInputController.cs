﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(CharacterMovement))]
public class PlayerInputController : MonoBehaviour
{
    private CharacterMovement characterController;

    private float startingPosition;

    private void Start()
    {
        characterController = GetComponent<CharacterMovement>();
    }

    private void FixedUpdate()
    {

#if UNITY_EDITOR
        if (Input.GetMouseButton(0))
        {
            characterController.ForwardMovement();
            characterController.SideControl();
        }

#elif UNITY_ANDROID
        if (Input.touchCount > 0)
        {
            Touch touch = Input.GetTouch(0);
            switch (touch.phase)
            {
                case TouchPhase.Began:
                    startingPosition = touch.position.x;
                    break;
                case TouchPhase.Moved:
                    characterController.SideControl();
                    characterController.ForwardMovement();
                    break;
                case TouchPhase.Ended:
                    Debug.Log("Touch Phase Ended.");
                    break;
            }
        }

#endif
    }


}
